The Bamboo Maven POM Value Extractor is an Atlassian Bamboo plugin that provides a 
build task that extracts values from Maven POMs and sets build variables 
using those values. This allows you to keep you Bamboo variables in sync with your 
Maven POM. You can automatically extract your artifact's GAV or "query" 
the POM to extract arbitrary values.  

Project Home Page and Wiki: https://gaptap.atlassian.net/wiki/display/BAMMVNEXTR
Issue Tracker: https://gaptap.atlassian.net/browse/BAMMVNEXTR
CI: https://gaptap.atlassian.net/builds/browse/BAMMVNEXTR
Atlassian Marketplace Listing: https://marketplace.atlassian.com/plugins/com.davidehringer.atlassian.bamboo.maven.maven-pom-parser-plugin